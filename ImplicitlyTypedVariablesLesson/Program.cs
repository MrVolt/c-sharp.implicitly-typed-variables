﻿using System;
using System.Collections.Generic;

namespace ImplicitlyTypedVariablesLesson
{
    class Program
    {
        static void Main(string[] args)
        {
            var number = 5;
            var name = "Anton";
            var isAdmin = false;

            var weekRation = new Dictionary<DayOfWeek, List<List<string>>>();

            // тип имя = значение;
            // var - variable (переменная)  - позволяет нам на этапе объявления и 
            // инициализации переменной не указывать полное название типа данных

            var age = 13;

            /*
             * code Python
             * 
             * a = "text"
             * a = 15
             * a = true
             * 
             */

            /* Для того, чтобы создать переменную, которая может принимать всё, что угодно, 
             * иначе говоря, динамически типизированную переменную, 
             * мы можем использовать ключевое слово dynamic */

            // dynamic имя = значение;
            dynamic something = 21;
            something = "";
            something = new List<string>();
        }
    }
}
